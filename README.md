# PROYECTO Nº3 PRÁCTICA GIT EVERIS

## Participantes

- ***Cristhian Javier Jaramillo Granda***
- [***Daniel Carmona Alarcón***](https://carmona44.github.io/portfolio/)

## Descripción

Proyecto sencillo de prueba enfocado a la práctica del controlador de versiones GIT perteneciente al curso impartido por [***Julián Hernández Carrión***](https://gitlab.com/esi_poo)
como parte de un conjunto de formaciones internas de Everis.

Consiste en pedir a un usuario que introduzca un número según las opciones que se le presenta en un menú por consola y a partir de ahí realiza 
una de las acciones ofrecidas como puede ser el listado de libros de una biblioteca, la editorial, listado de actores...

## Agradecimientos

Todos los agradecimientos por parte de este equipo al profesor Julián por enseñarnos tanto y compartir su conocimiento con los demás.

**¡Viva el Open Source!**